import Cookies from "universal-cookie";

const cookies = new Cookies();

const initialState = {
  token: cookies.get('LesPtitsGourmetsApiToken'),
  user: cookies.get('LesPtitsGourmetsUser'),
}

const authReducer = (state = initialState, action) => {
  switch(action.type){
    // saves the token into the state
    case 'AUTH_SET_TOKEN':
      window.location.reload();
      return Object.assign({}, state, {
        token: action.obj
      })
    // discards the current token (logout)
    case 'AUTH_DISCARD_TOKEN':
      window.location.reload();
      cookies.remove('LesPtitsGourmetsApiToken');
      cookies.remove('LesPtitsGourmetsUser');
      return {};
    // as always, on default do nothing
    default:
      return state;
  }
}

export default authReducer;
