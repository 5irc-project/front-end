import { combineReducers } from 'redux';
import recipeReducer from './recipeReducer';
import authReducer from './authReducer';
import recipesListReducer from './recipesListReducer';

const globalReducer  = combineReducers({
  recipeReducer: recipeReducer,
  authReducer: authReducer,
  recipesListReducer: recipesListReducer,
});

export default globalReducer;
