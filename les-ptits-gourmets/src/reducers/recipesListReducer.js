const recipesListReducer= (state={}, action) => {
  switch (action.type) {
    case 'SET_RECIPES_LIST':
      return Object.assign({}, state, {
        recipes_list: action.obj
      })
    default:
      return state;
  }
}
export default recipesListReducer;
