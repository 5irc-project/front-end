const recipeReducer= (state={}, action) => {
  switch (action.type) {
    case 'SET_SELECTED_RECIPE':
      return Object.assign({}, state, {
        recipe: action.obj
      })
    case 'UPDATE_CREATED_RECIPE':
      return Object.assign({}, state, {
        created_recipe: action.obj
      })
    default:
      return state;
  }
}

export default recipeReducer;
