export const setSelectedRecipe=(recipe_obj)=>{
  return {
    type: 'SET_SELECTED_RECIPE',
    obj: recipe_obj
  }
}

export const updateCreatedRecipe=(recipe_obj)=>{
  return {
    type: 'UPDATE_CREATED_RECIPE',
    obj: recipe_obj
  }
}

export const authSetToken=(token_obj)=>{
  return {
    type: 'AUTH_SET_TOKEN',
    obj: token_obj
  };
}

export const authDiscardToken=()=>{
  return {
    type: 'AUTH_DISCARD_TOKEN'
  };
}

export const setRecipesList=(recipes_list)=>{
  return {
    type: 'SET_RECIPES_LIST',
    obj: recipes_list
  };
}
