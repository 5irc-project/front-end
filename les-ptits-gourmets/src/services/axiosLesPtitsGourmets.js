import axios from 'axios';

export default axios.create({
  baseURL: `https://les-ptits-gourmets.herokuapp.com`
});
