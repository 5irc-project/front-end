import axios from './axiosLesPtitsGourmets';
import { connect } from 'react-redux';

export default {
  recipes() {
    return {
      // Get all recipes in database
      getAll: (auth_token) => axios.get('/recipes', (auth_token) && {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Filter recipes by tagId
      tagFilter: (tagId, auth_token) => axios.get('/recipes/tag/'+tagId, (auth_token) && {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Filter results by name containing nameEntered
      nameFilter: (nameEntered, auth_token) => axios.get('/recipes', (auth_token) ? {
        params: {
          name: nameEntered
        },
        headers: {
          Authorization: auth_token,
        }
      } : {
        params: {
          name: nameEntered
        }
      }),

      // Get the recipe with this id
      get: (id, auth_token) => axios.get('/recipe/'+id, (auth_token) && {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Create the toCreate recipe
      create: (toCreate, auth_token) => axios.post('/recipe', toCreate, (auth_token) && {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Like a recipe
      like: (recipe,auth_token) => axios.post('/recipe/like', recipe, {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Dislike a recipe
      dislike: (recipe, auth_token) => axios.post('/recipe/dislike', recipe, {
        headers: {
          Authorization: auth_token,
        }
      }),

      // Delete a recipe by its id
      delete: (id, auth_token) => axios.delete('/recipe/'+id, {
        headers: {
          Authorization: auth_token,
        }
      })
    }
  },
  ingredients() {
    return {
      getAll: () => axios.get('/ingredients'),
      create: (toCreate, auth_token) => axios.post('/ingredient', toCreate, (auth_token) && {
        headers: {
          Authorization: auth_token,
        }
      }),
    }
  },
  units() {
    return {
      getAll: () => axios.get('/units'),
    }
  },
  tags() {
    return {
      getAll: () => axios.get('/tags'),
    }
  },
  user() {
    return {
      login: (credentials) => axios.post('/login', credentials),
      signup: (user) => axios.post('/users/register', user),
    }
  },
}
