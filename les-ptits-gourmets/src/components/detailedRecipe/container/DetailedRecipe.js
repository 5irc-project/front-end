import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setSelectedRecipe, setRecipesList } from '../../../actions';
import styles from './DetailedRecipe.module.css';
import cn from "classnames";
import * as recipeTmp from '../../../source/exemple_recipe.json';

import DisplayTags from '../components/DisplayTags';
import Rating from '../../common/Rating';
import DisplayIngredients from '../components/DisplayIngredients';
import DisplaySteps from '../components/DisplaySteps';
import DisplayDescription from '../components/DisplayDescription';
import API from '../../../services/api.js';

class DetailedRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailedRecipe: null,
      durationPreparation: 0,
      durationCooking: 0,
      durationRest: 0,
      isLiked: false,
    }
    this.handleBackButtonPressed = this.handleBackButtonPressed.bind(this);
    this.getRecipe = this.getRecipe.bind(this);
    this.calculateDuration = this.calculateDuration.bind(this);
    this.deleteRecipe = this.deleteRecipe.bind(this);
  }

  componentDidMount() {
    this.getRecipe();
  }

  getRecipe() {
    API.recipes().get(this.props.recipe.id, this.props.auth_token).then(res => {
      this.setState({detailedRecipe: res.data.data[0]},this.calculateDuration);
    })
  }

  calculateDuration() {
    let durationPreparation = 0;
    let durationCooking = 0;
    let durationRest = 0;
    if (this.state.detailedRecipe.steps) {
      this.state.detailedRecipe.steps.forEach(
        step => {
          if (step.duration) {
            if (step.type === "preparation")
            {
              durationPreparation += step.duration;
            }
            else if (step.type === "cooking")
            {
              durationCooking += step.duration;
            }
            else if (step.type === "rest")
            {
              durationRest += step.duration;
            }
          }
        }
      );
    }
    this.setState({
      durationPreparation,
      durationCooking,
      durationRest,
    })
  }

  handleBackButtonPressed(){
    API.recipes().getAll(this.props.auth_token).then(res => {
      this.props.dispatch(setRecipesList(res.data.data));
      this.props.dispatch(setSelectedRecipe(null));
    })
  }

  deleteRecipe(e){
    API.recipes().delete(this.state.detailedRecipe.id, this.props.auth_token).then(res => {
      window.location.reload();
    }).catch(err => {
      alert("Vous n'avez pas le droit de supprimer cette recette");
    })
  }

  render() {
    if(this.state.detailedRecipe)
    {
      let styleImage;
      if (this.state.detailedRecipe.photoPath) {
        styleImage = {
          backgroundImage: 'url(' + this.state.detailedRecipe.photoPath + ')',
        }
      }
      return(
        <div className="container m-2">
          <div className="row vh-40">
            <span className={cn(styles.previous, "m-2 p-2")} onClick={this.handleBackButtonPressed}>&laquo; Retour</span>
            <div className={styles.img} role="img" aria-label={this.state.detailedRecipe.name} style={styleImage}/>
          </div>
          <div className="row pt-2">
            <div className="col-9">
              <div className="row">
                <h2 className="col-10">{this.state.detailedRecipe.name}</h2>
                {/*<span href="col-1">
                  {this.state.isLiked ? (
                    <img src="red_heart.png" onClick={() => this.setState({isLiked:false})}/>
                  ) : (
                    <img src="empty_heart.png" onClick={() => this.setState({isLiked:true})}/>
                  )}
                </span>*/}
                <div className="dropdown col-1">
                  <button className="btn border-0 bg-white" type="button" id="dropdownMoreButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="baseline_more_horiz_black_18dp.png"/>
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMoreButton">
                    <a className="dropdown-item" onClick={this.deleteRecipe}>Supprimer</a>
                  </div>
                </div>
              </div>
              <div className="row justify-content-between">
                <div className="col-1">
                  <Rating score={this.state.detailedRecipe.score} recipe={this.state.detailedRecipe}/>
                </div>
                <div className="col-10">
                  <DisplayDescription description={this.state.detailedRecipe.description}
                    durationPreparation={this.state.durationPreparation}
                    durationCooking={this.state.durationCooking}
                    durationRest={this.state.durationRest}/>
                </div>
              </div>
            </div>
            <div className="col-3">
              <DisplayTags tags={this.state.detailedRecipe.tagRecipes}/>
            </div>
          </div>

          <div className="row">
            <div className="col-3">
              <DisplayIngredients ingredients={this.state.detailedRecipe.ingredientrecipe}/>
            </div>
            <div className="col-9">
              <DisplaySteps steps={this.state.detailedRecipe.steps}/>
            </div>
          </div>
        </div>
      );
    }
    else {
      return(<div/>);
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    recipe: state.recipeReducer.recipe,
    auth_token: state.authReducer.token
  };
}

export default connect(mapStateToProps)(DetailedRecipe);
