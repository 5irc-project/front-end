import React, { Component } from 'react';

class DisplayTags extends Component {
  constructor(props){
    super(props);
  }

  render() {
    let displayTag = [];
    if (this.props.tags){
      displayTag = this.props.tags.map(
        tag => {
          return <li key={tag.tag.id}>{tag.tag.name}</li>;
        }
      );
    }
    return(
      <div>
        <h5>Thèmes : </h5>
        <ul>
          {displayTag}
        </ul>
      </div>
    );
  }
}
export default DisplayTags;
