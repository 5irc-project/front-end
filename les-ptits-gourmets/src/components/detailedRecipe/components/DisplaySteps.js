import React, { Component } from 'react';

class DisplaySteps extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let displaySteps = [];
    if (this.props.steps){
      displaySteps= this.props.steps.map(
        steps => {
          return <li className="list-group-item" key={steps.noorder}>{steps.noorder}) {steps.description}</li>;
        }
      );
    }
    return(
      <div className="pt-3">
        <h4>Etapes : </h4>
        <ul className="list-group">
          {displaySteps}
        </ul>
      </div>
    );
  }
}
export default DisplaySteps;
