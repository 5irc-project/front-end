import React, { Component } from 'react';

class DisplayIngredients extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let displayIngredients = []
    if (this.props.ingredients) {
      displayIngredients = this.props.ingredients.map(
        ingredient => {
          return <li key={ingredient.ingredient.id}>{ingredient.ingredient.name} : {ingredient.quantity} {ingredient.ingredient.unit.name}</li>;
        }
      );
    }
    return(
      <div className="pt-3">
        <h4>Ingrédients : </h4>
        <ul>
          {displayIngredients}
        </ul>
      </div>
    );
  }
}
export default DisplayIngredients;
