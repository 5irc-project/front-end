import React, { Component } from 'react';
import styles from "../container/DetailedRecipe.module.css";
import cn from "classnames";

class DisplayDescription extends Component {
  constructor(props){
    super(props);
    this.state= {
      preparation: "",
      cooking: "",
      rest: ""
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.durationPreparation !== this.props.durationPreparation ||
      prevProps.durationCooking !== this.props.durationCooking ||
      prevProps.durationRest !== this.props.durationRest)
    {
      this.setState({
        preparation: this.timeConvert(this.props.durationPreparation),
        cooking: this.timeConvert(this.props.durationCooking),
        rest: this.timeConvert(this.props.durationRest)
      })
    }
  }

  timeConvert(n) {
    let num = n;
    let hours = (num / 60);
    let rhours = Math.floor(hours);
    let minutes = (hours - rhours) * 60;
    let rminutes = Math.round(minutes);
    return rhours + "h " + rminutes + "min";
  }

  render() {
    return (
      <div>
        <p className="row m-0">
          {this.props.description}
        </p>
        <div className="row pt-4">
          {this.props.durationPreparation !== 0 &&
            <div className={cn("col", styles.border)}>
              <p className="mb-0">Temps de préparation : {this.state.preparation}</p>
            </div>
          }
          {this.props.durationCooking !== 0 &&
            <div className={cn("col", styles.border)}>
              <p className="mb-0">Temps de cuisson : {this.state.cooking}</p>
            </div>
          }
          {this.props.durationRest !== 0 &&
            <div className={cn("col", styles.border)}>
              <p className="mb-0">Temps de préparation : {this.state.rest}</p>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default DisplayDescription;
