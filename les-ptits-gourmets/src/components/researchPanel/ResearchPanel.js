import React, { Component } from 'react';
import cn from 'classnames';
import styles from './ResearchPanel.module.css';
import * as recipesTmp from '../../source/exemple_list_recipe.json';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { connect } from "react-redux";
import { setRecipesList, setIsRecipesListOk } from "../../actions";

import API from '../../services/api.js';
import RecipeCreation from '../recipeCreation/container/RecipeCreation';
import Recipe from '../recipe/container/Recipe';

class ResearchPanel extends Component {
  constructor(props){
    super(props);
    this.state = {
      recipes: null,
      recipeCreation: false,
    };
    this.getRecipes = this.getRecipes.bind(this);
    this.changeCreationStatus = this.changeCreationStatus.bind(this);
  }

  componentDidMount(){
    this.getRecipes();
  }

  getRecipes(){
    API.recipes().getAll(this.props.auth_token).then(res => {
      this.props.dispatch(setRecipesList(res.data.data));
    });
  }

  changeCreationStatus() {
    this.setState({
      recipes: [],
      recipeCreation: !this.state.recipeCreation,
    },this.getRecipes);
  }

  render() {
    let displayChildren;
    // If the data is not loaded, display a loader
    if (this.props.recipes_list===undefined) {
      return (
        <div className="border-top h-100">
          <div className="row h-100 align-items-center justify-content-center">
            <div className="col-12 col-md-1 align-self-center">
              <div className={styles.loader} />
            </div>
          </div>
        </div>
      )
    }
    // when the data is loaded, display a list of recipes
    else if (this.state.recipeCreation === false){
      // creation of a list of recipe that display the short display of each recipe
      if (this.props.recipes_list) {
        displayChildren = this.props.recipes_list.map(
          recipe => {
            return <Recipe containedRecipe={recipe} key={recipe.id}/>
          });
      }
      return (
        <div className={cn(styles.verticallyScrollable,"border-top h-100")}>
          <div className="row m-3">
            {displayChildren}
          </div>
          {this.props.auth_token && (
            <Fab
              color="primary"
              aria-label="Add"
              className={styles.addButton}
              onClick={this.changeCreationStatus}
            >
              <AddIcon/>
            </Fab>
          )}
        </div>
      );
    }
    else if (this.state.recipeCreation === true){
      return (
        <RecipeCreation changeCreationStatus={this.changeCreationStatus}/>
      );
    }
  }
}


const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token,
    recipes_list: state.recipesListReducer.recipes_list,
  };
}

export default connect(mapStateToProps)(ResearchPanel);
