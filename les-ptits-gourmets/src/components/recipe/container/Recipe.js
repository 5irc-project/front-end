import React, { Component } from 'react';
import ShortRecipe from '../components/ShortRecipe';
import DetailedRecipe from '../../detailedRecipe/container/DetailedRecipe';
import { connect } from 'react-redux';

class Recipe extends Component {
  constructor(props){
    super(props);
  }

  render() {
    // If the component displays detailed recipe
    if (this.props.recipe) {
      if (this.props.containedRecipe.id === this.props.recipe.id) {
        return (
          <DetailedRecipe/>
        );
      }
      else {
        return(<div/>);
      }
    }
    // If the components display a short recipe
    else {
      return (
        <ShortRecipe containedRecipe={this.props.containedRecipe}/>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    recipe: state.recipeReducer.recipe
  };
}

export default connect(mapStateToProps)(Recipe);
