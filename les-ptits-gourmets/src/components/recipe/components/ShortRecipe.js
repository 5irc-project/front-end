import React, { Component } from 'react';
import styles from '../container/Recipe.module.css';
import { connect } from 'react-redux';
import { setSelectedRecipe } from '../../../actions';
import cn from "classnames";

import Rating from "../../common/Rating";

class ShortRecipe extends Component {
  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    this.props.dispatch(setSelectedRecipe(this.props.containedRecipe));
  }

  render() {
    let styleImage;
    if (this.props.containedRecipe.photoPath) {
      styleImage = {
        backgroundImage: 'url(' + this.props.containedRecipe.photoPath + ')',
      }
    }
    return(
      <div className="col-12 col-md-4 col-lg-4 col-sm-4 p-2" onClick={this.handleClick}>
        <div className={cn("card p-2", styles.card)}>
          {/* Set the image as background in order to crop the image */}
          <div className="card-img-top">
            <div className={styles.img} role="img" aria-label={this.props.containedRecipe.name} style={styleImage}/>
          </div>
          <div className="card-body pl-2 pr-2 row">
            <div className="col-2 pl-2 pr-0">
              <Rating score={this.props.containedRecipe.score} recipe={this.props.containedRecipe}/>
            </div>
            {/* Body of the Card */}
            <div className="col-10">
              <h5 className="card-title">{this.props.containedRecipe.name}</h5>
              <p className="card-text">{this.props.containedRecipe.description}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(ShortRecipe);
