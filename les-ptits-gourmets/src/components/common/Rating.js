import React, { Component } from 'react';
import { connect } from "react-redux";
import cn from "classnames";

import API from '../../services/api.js';

class Rating extends Component {
  constructor(props) {
    super(props);
    this.state= {
      upVotePressed: (this.props.recipe && this.props.recipe.scoreRecipes && this.props.recipe.scoreRecipes[0].value == 1),
      downVotePressed: (this.props.recipe && this.props.recipe.scoreRecipes && this.props.recipe.scoreRecipes[0].value == -1),
      note: this.props.score,
    }
    this.isDisabled = this.isDisabled.bind(this);
    this.isLiked = this.isLiked.bind(this);
    this.isDisliked = this.isDisliked.bind(this);
  }

  isDisabled() {
    if(this.props.isDisabled == 'true') {
      return "disabled";
    }
    else {
      if (this.props.auth_token) {
        return "";
      }
      else  {
        return "disabled";
      }
    }
  }

  isLiked(e) {
    e.stopPropagation();
    let recipe= {
      "recipe": {
        id: this.props.recipe.id,
      }
    };
    API.recipes().like(recipe,this.props.auth_token).then(res => {
      this.setState({
        upVotePressed: true,
        note: this.state.note+1,
        //note: res.data.data[0].recipe.score,
      })
    });
  }

  isDisliked(e) {
    e.stopPropagation();
    let recipe= {
      "recipe":{
        id: this.props.recipe.id,
      }
    };
    API.recipes().dislike(recipe, this.props.auth_token).then(res => {
      this.setState({
        downVotePressed: true,
        note: this.state.note-1,
        //note: res.data.data[0].recipe.score,
      })
    })
  }

  render() {
    const upVoteButton = this.state.upVotePressed ? "btn-success" : "btn-light";
    const downVoteButton = this.state.downVotePressed ? "btn-danger" : "btn-light";
    const disable = (this.state.upVotePressed || this.state.downVotePressed || this.isDisabled()) && ("disabled");
    return (
      <div className="justify-content-md-center">
        <a href="#" role="button" className={cn("align-self-center col-12 p-0 btn", upVoteButton, disable)} onClick={this.isLiked}>
          ▲
        </a>
        {/* Display the score in green if positive, red if negative, white if 0 */}
        {this.state.note == 0 ? (
          <p className="col-12 text-center p-0 mt-1 mb-1">{this.state.note}</p>
        ) : this.state.note > 0 ? (
            <p className="col-12 text-center p-0 mt-1 mb-1 text-success">{this.state.note}</p>
          ) : (
            <p className="col-12 text-center p-0 mt-1 mb-1 text-danger">{this.state.note}</p>
          )
        }
        <a href="#" role="button" className={cn("align-self-center col-12 p-0 btn", downVoteButton, disable)} onClick={this.isDisliked}>
          ▼
        </a>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token
  };
}

export default connect(mapStateToProps)(Rating);
