import React, { Component } from 'react';
import styles from './Main.module.css';
import cn from "classnames";
import Navbar from '../navbar/container/Navbar';
import LeftPanel from '../leftPanel/LeftPanel';
import ResearchPanel from '../researchPanel/ResearchPanel';
import { Provider } from 'react-redux';
import globalReducer from '../../reducers';
import { createStore } from 'redux';

const store = createStore(globalReducer);

class Main extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Provider store={store}>
        <div className={styles.fullWidth}>
          <Navbar/>
          <div className={cn(styles.container,"container-fluid")}>
            <div className="row h-100">
              <div className="col-2 p-0">
                <LeftPanel/>
              </div>
              <div className="col-10 p-0">
                <ResearchPanel/>
              </div>
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}

export default Main;
