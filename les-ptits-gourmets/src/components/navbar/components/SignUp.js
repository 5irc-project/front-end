import React, { Component } from 'react';
import cn from "classnames";
import { connect } from "react-redux";
import { authSetToken } from "../../../actions";
import Cookies from "universal-cookie";

import API from '../../../services/api.js';

const cookies = new Cookies();

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state= {
      username:"",
      email:"",
      password:"",
      passwordConfirmation:"",
    }
    this.signUp = this.signUp.bind(this);
  }

  signUp(e) {
    e.preventDefault();
    // appel backend
    let user = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    }
    API.user().signup(user).then((response) => {
      API.user().login(user).then(res => {
        // 1 year of availability
        let cookieDuration = 1 * 365 * 24 * 3600;
        cookies.set('LesPtitsGourmetsApiToken', res.headers.authorization, { path : '/', maxAge : cookieDuration });
        this.props.dispatch(authSetToken(res.headers.authorization));

      })
    });
  }

  isValid(bool) {
    if (!bool){
      return "is-invalid";
    }
  }

  render() {
    const {username, email, password, passwordConfirmation} = this.state;
    const isPasswordConfirmed = password === passwordConfirmation && password.length > 0;
    const isEnabled = username.length > 0 && email.length > 0 && isPasswordConfirmed;
    return (
      <div className="modal fade" id="signUpModal" tabIndex="-1" role="dialog" aria-labelledby="signUpModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">S'enregistrer</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body container form-group">
              <div className="form-group">
                <input type="text" placeholder="Nom d'utilisateur" className="form-control mb-3" onChange={e => this.setState({username: e.target.value})} required/>
                <input type="email" placeholder="Adresse mail" className="form-control mb-3" onChange={e => this.setState({email: e.target.value})} required/>
                <input type="password" placeholder="Mot de passe" className="form-control mb-3" onChange={e => this.setState({password: e.target.value})} required/>
                <input type="password" placeholder="Validation du mot de passe" className={cn(this.isValid(isPasswordConfirmed),"form-control mb-3")} onChange={e => this.setState({passwordConfirmation: e.target.value})} required/>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <button type="button" className="btn btn-primary" onClick={this.signUp} data-dismiss="modal" disabled={!isEnabled}>S'enregistrer</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token
  };
}

export default connect(mapStateToProps)(SignUp);
