import React, { Component } from 'react';
import cn from "classnames";
import { connect } from "react-redux";
import { authSetToken } from "../../../actions";
import Cookies from "universal-cookie";

import API from '../../../services/api.js';

const cookies = new Cookies();

class Login extends Component {
  constructor(props) {
    super(props);
    this.state= {
      email:"",
      password:"",
    }
    this.login = this.login.bind(this);
  }

  login(e) {
    e.preventDefault();
    // appel backend
    API.user().login(this.state).then(res => {
      // 1 year of availability
      let cookieDuration = 1 * 365 * 24 * 3600;
      cookies.set('LesPtitsGourmetsApiToken', res.headers.authorization, { path : '/', maxAge : cookieDuration });
      this.props.dispatch(authSetToken(res.headers.authorization));
    })
  }

  isValid(bool) {
    if (!bool){
      return "is-invalid";
    }
  }

  render() {
    const {email, password} = this.state;
    const isEnabled = email.length > 0 && password.length > 0;
    return (
      <div className="modal fade" id="loginModal" tabIndex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Se connecter</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body container form-group">
              <div className="form-group">
                <input type="email" placeholder="Adresse mail" className="form-control mb-3" onChange={e => this.setState({email: e.target.value})} required/>
                <input type="password" placeholder="Mot de passe" className="form-control mb-3" onChange={e => this.setState({password: e.target.value})} required/>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <button type="button" className="btn btn-primary" onClick={this.login} data-dismiss="modal" disabled={!isEnabled}>Se connecter</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token
  };
}

export default connect(mapStateToProps)(Login)
