import React, { Component } from 'react';
import styles from './Navbar.module.css';
import { connect } from "react-redux";
import { authDiscardToken, setRecipesList, setSelectedRecipe } from "../../../actions";
import API from "../../../services/api.js";

import SignUp from '../components/SignUp';
import Login from '../components/Login';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state={
      research:''
    }
    this.logout = this.logout.bind(this);
    this.getAllRecipes = this.getAllRecipes.bind(this);
    this.research = this.research.bind(this);
  }

  logout() {
    this.props.dispatch(authDiscardToken());
  }

  getAllRecipes() {
    this.props.dispatch(setSelectedRecipe(null));
    this.props.dispatch(setRecipesList(undefined));
    API.recipes().getAll(this.props.auth_token).then(res => {
      this.props.dispatch(setRecipesList(res.data.data));
    })
  }

  research(e) {
    e.preventDefault();
    this.props.dispatch(setSelectedRecipe(null));
    this.props.dispatch(setRecipesList(undefined));
    API.recipes().nameFilter(this.state.research, this.props.auth_token).then(res => {
      if (res.status === 204) {
        this.props.dispatch(setRecipesList([]))
      }
      else {
        this.props.dispatch(setRecipesList(res.data.data));
      }
      this.setState({
        research:'',
      });
    })
  }

  render() {
    return (
      <div>
        <nav className="navbar sticky-top navbar-expand-lg navbar-light bg-light pt-0 pb-0">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navBarToggler" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <a className="navbar-brand" href="#" onClick={this.getAllRecipes}>
            <img src="/favicon.ico" width="35" height="50" className="d-inline-block" alt="Les P'tits Gourmets"/>
            Les P'tits Gourmets
          </a>

          {/* Collapsable part of the navbar */}
          <div className="collapse navbar-collapse" id="navBarToggler">
            <form className="form-inline my-2 my-lg-0 mx-auto">
              <input className="form-control mr-sm-2" type="search" placeholder="..." value={this.state.research} onChange={(e)=>this.setState({research:e.target.value})}/>
              <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={this.research}>Rechercher</button>
            </form>
              {this.props.auth_token ? (
                <ul className="navbar-nav ml-auto">

                  <li className="nav-item">
                    <a className="nav-link disabled" href="#">Liste de favoris</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" onClick={this.logout}>Se déconnecter</a>
                  </li>
                </ul>
              ) : (
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="modal" data-target="#signUpModal">S'enregistrer</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="modal" data-target="#loginModal">Se connecter</a>
                  </li>
                </ul>
              )}
          </div>
        </nav>
        <SignUp/>
        <Login/>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token
  };
}

export default connect(mapStateToProps)(Navbar);
