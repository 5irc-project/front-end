import React, { Component } from "react";
import cn from "classnames";
import styles from "../container/RecipeCreation.module.css";

import API from '../../../services/api.js';

class TagSelection extends Component {
  constructor() {
    super();
    this.state = {
      availableTags: [],
      addedTags: []
    }
    this.onAddition = this.onAddition.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.getListTags();
  }

  getListTags() {
    API.tags().getAll().then(res => {
      this.setState({availableTags: res.data.data})
    });
  }

  onAddition(e) {
    e.preventDefault();
    let value = document.getElementById('tagSelect').value;
    let newValue = {
      id: value,
      name: '',
    };
    if(value !== "-1") {
      let newAvailableTagsList = this.state.availableTags.filter(function(item){
        if (item.id == value) {
          newValue.name = item.name;
        }
        return item.id != value;
      });
      this.setState(state => {
        const addedTags = state.addedTags.concat({
          tag: {
            name: newValue.name,
            id: newValue.id
          }
        });
        const availableTags = newAvailableTagsList;
        return {
          addedTags,
          availableTags,
        };
      }, this.onChange);
    }
    document.getElementById('tagSelect').value = "0";
  }

  removeTag(e, tag) {
    e.preventDefault();
    let newAddedTagsList = this.state.addedTags.filter(function(item){
      return item.id != tag.id;
    });
    this.setState(state => {
      const availableTags = state.availableTags.concat({
        name: tag.tag.name,
        id: tag.tag.id,
      });
      const addedTags = newAddedTagsList;
      return {
        availableTags,
        addedTags,
      };
    }, this.onChange);
  }

  onChange() {
    if(this.state.addedTags) {
      let recipe = {
        target: {
          value: this.state.addedTags,
        }
      };
      this.props.onChange(recipe, "tagRecipes");
    }
  }

  render() {
    let displayAvailableTags = "";
    let displayTags = "";
    if(this.state.availableTags) {
      displayAvailableTags= this.state.availableTags.map(tag => {
        return <option key={tag.id} value={tag.id}>{tag.name}</option>
      });
    }
    if(this.props.tags) {
      displayTags = this.props.tags.map(tag => {
        return (
          <li key={tag.tag.id}>
            {tag.tag.name}
            <span className={cn(styles.removeTag, "badge badge-danger float-right")} onClick={e => this.removeTag(e, tag)}>-</span>
          </li>);
      });
    }
    return (
      <div>
        <h5>Thèmes : </h5>
        <ul>
          {displayTags}
        </ul>
        <select className="custom-select col-10" id="tagSelect">
          <option value="-1" defaultValue>Choisissez un ou plusieurs thèmes</option>
          {displayAvailableTags}
        </select>
        <button
          type="submit"
          aria-label="Add"
          className="btn btn-primary col-2"
          onClick={this.onAddition}
        >+</button>
      </div>
    );
  }
}

export default TagSelection;
