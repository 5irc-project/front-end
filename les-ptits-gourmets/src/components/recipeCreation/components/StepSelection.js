import React, { Component } from "react";

class StepSelection extends Component {
  constructor(props){
    super(props);
    this.state = {
      noorder: 1,
      duration: "",
      description: "",
      type: "preparation"
    }
    this.setStep = this.setStep.bind(this);
  }

  setStep() {
    let noorder = this.state.noorder+1;
    let newStepList = [];
    if (this.props.steps) {
      newStepList = this.props.steps;
    }
    newStepList.push({
      noorder: this.state.noorder,
      description: this.state.description,
      duration: this.state.duration,
      type: this.state.type
    });
    let recipe = {
      target: {
        value: newStepList
      }
    }
    this.props.onChange(recipe, "steps");
    this.setState({
      duration: "",
      noorder: noorder,
      description: "",
    })
  }

  render() {
    let displaySteps = "";
    if(this.props.steps) {
      displaySteps = this.props.steps.map(
        steps => {
          return <li className="list-group-item" key={steps.noorder}>{steps.noorder}) {steps.description}</li>;
        }
      );
    }
    return(
      <div>
        <div>
          <h4>Etapes : </h4>
          {displaySteps}
          <button type="button" className="btn btn-success" data-toggle="modal" data-target="#stepModal">
            Ajouter une étape
          </button>
        </div>

        <div className="modal fade" id="stepModal" tabIndex="-1" role="dialog" aria-labelledby="stepModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Ajoutez votre étape</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body container form-group">
                <textarea className="form-control" id="descriptionInput" placeholder="Description de l'étape*" rows="2" resize="none"
                  value={this.state.description} onChange={(e) => this.setState({description: e.target.value})}/>
                <input className="small form-control form-control-md mt-2 mb-2" id="durationInput" type="number" value={this.state.duration} placeholder="Durée de l'étape (min)" onChange={e => this.setState({duration: e.target.value})}/>
                <label htmlFor="typeSelect">
                  Selectionnez le type d'étape :*
                  <select className="custom-select col-10" id="typeSelect" onChange={e => this.setState({type: e.target.value})}>
                    <option value="preparation">Préparation</option>
                    <option value="cooking">Cuisson</option>
                    <option value="waiting">Attente</option>
                  </select>
                </label>

              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" className="btn btn-primary" onClick={this.setStep} data-dismiss="modal">Ajouter cette étape</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
export default StepSelection;
