import React, { Component } from "react";
import Select from 'react-select';

import NewIngredient from "./NewIngredient";
import API from '../../../services/api.js';

class IngredientSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listIngredients: [],
      displayedListIngredients: [],
      newIngredientRecipe:{},
      selectedIngredient:{},
    }
    this.createDisplayedListIngredients = this.createDisplayedListIngredients.bind(this);
    this.ingredientSelected = this.ingredientSelected.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.setIngredient = this.setIngredient.bind(this);
    this.getListIngredients = this.getListIngredients.bind(this);
  }

  componentDidMount() {
    this.getListIngredients();
  }

  addIngredient(ingredient) {
    let listIngredients = this.state.listIngredients;
    listIngredients.push(ingredient);
    this.setState(listIngredients, this.createDisplayedListIngredients);
  }

  getListIngredients() {
    API.ingredients().getAll().then(res => {
      this.setState({
        listIngredients: res.data.data
      }, this.createDisplayedListIngredients)
    });
  }

  createDisplayedListIngredients() {
    let displayedListIngredients = [];
    this.state.listIngredients.map(ingredient => {
      let newIngredient = {
        label: ingredient.name,
        value: ingredient.id,
      }
      displayedListIngredients.push(newIngredient);
    })
    this.setState({displayedListIngredients});
  }

  ingredientSelected(e) {
    let newIngredientRecipe = Object.assign({name: e.label}, {id: e.value}, this.state.newIngredient);
    let selectedIngredient = this.state.listIngredients.filter(function(item){
      return item.id === e.value;
    });
    this.setState({newIngredientRecipe, selectedIngredient});
  }

  setIngredient() {
    let newIngredientList = [];
    // if it's not the first ingredient we pick up the list in props
    if (this.props.ingredients){
      newIngredientList = this.props.ingredients;
    }
    // push this new ingredient to the array
    newIngredientList.push({
      ingredient: {
        id: this.state.selectedIngredient[0].id,
        name: this.state.selectedIngredient[0].name,
        unit: this.state.selectedIngredient[0].unit
      },
      quantity: this.state.quantity
    });
    let recipe = {
      target: {
        value: newIngredientList
      }
    }
    this.props.onChange(recipe, "ingredientrecipe");
    document.getElementById("quantityInput").value = "";
    this.setState({
      quantity: "",
    })
  }

  render() {
    let ingredientList;
    if(this.props.ingredients) {
      ingredientList = this.props.ingredients.map(ingredient => {
        return <li key={ingredient.ingredient.name}>{ingredient.ingredient.name} : {ingredient.quantity} {ingredient.ingredient.unit.name}</li>;
      })
    }
    return (
      <div>
        <div>
          <h4>Ingrédients : </h4>
          <ul>
            {ingredientList}
          </ul>
          <button type="button" className="btn btn-success" data-toggle="modal" data-target="#ingredientsModal">
            Ajouter un ingrédient
          </button>
        </div>

        <div className="modal fade" id="ingredientsModal" tabIndex="-1" role="dialog" aria-labelledby="ingredientsModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Sélectionnez votre ingrédient</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body container form-group">
                <div className="row mr-2 ml-2">
                  <Select
                    className="basic-single col-10 p-0"
                    isClearable={true}
                    name="ingredient"
                    options={this.state.displayedListIngredients}
                    placeholder="Choisissez votre ingrédient"
                    onChange={this.ingredientSelected}
                  />
                  <button
                    type="submit"
                    aria-label="Add"
                    className="btn btn-success col-2"
                    data-toggle="modal"
                    data-target="#newIngredientModal"
                    data-dismiss="modal"
                  >+</button>
                </div>
                <div className="row mr-2 ml-2 pt-2">
                  <input className="small form-control form-control-md col-10" id="quantityInput" type="number" placeholder="Quantité" onChange={e => this.setState({quantity: e.target.value})}/>
                  <div className="col-2 p-0 align-self-center">
                    {this.state.selectedIngredient[0] && (
                      <p className="pl-1 m-0">{this.state.selectedIngredient[0].unit.name}</p>
                    )}
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" className="btn btn-primary" onClick={this.setIngredient} data-dismiss="modal">Ajouter cet ingrédient</button>
              </div>
            </div>
          </div>
        </div>

        <NewIngredient addIngredient={this.addIngredient}/>

      </div>
    )
  }
}

export default IngredientSelection;
