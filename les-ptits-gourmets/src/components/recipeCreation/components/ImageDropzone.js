import React, { Component } from "react";
import cn from 'classnames';
import styles from '../container/RecipeCreation.module.css';
import { connect } from 'react-redux';

class ImageDropzone extends Component {
  constructor(props){
    super(props);
    this.state = {
      photoPath: "",
      preview:false,
    }
    this.imageChange = this.imageChange.bind(this);
  }

  imageChange(e) {
    let recipe = {
      target: {
        value: e.target.value,
      }
    }
    this.props.onChange(recipe,"photoPath");
    this.setState({
      photoPath:e.target.value,
      preview:false
    });
  }

  render() {
    let styleImage = {
      backgroundImage: 'url()'
    };
    if (this.state.preview){
      styleImage = {
        backgroundImage: 'url(' + this.state.photoPath + ')',
      };
    }
    return(
      <section className="h-100 w-100 border container p-0">
        <div className={styles.dropzone}>
          <div className={styles.img} role="img" aria-label="new-file" style={styleImage}/>
          <div className="row w-50 mx-auto">
            <div className="col-9 pr-0">
              <input type="text" className="form-control w-100" placeholder="URL de l'image" aria-label="URL of the image" onChange={this.imageChange}/>
            </div>
            <div className="col-3 pl-0">
              <button type="button" className="btn-dark pt-1 pb-1 w-100" onClick={() => this.setState({preview:true})}>Prévisualiser</button>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default ImageDropzone;
