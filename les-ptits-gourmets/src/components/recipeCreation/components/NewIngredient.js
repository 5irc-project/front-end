import React, { Component } from "react";
import { connect } from 'react-redux';

import API from '../../../services/api.js';

class NewIngredient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      unitList: [],
      name:"",
      kcal:"",
      price:"middle",
      unit:"unité",
      unit_id:1,
      id:0
    }
    this.createNewIngredient = this.createNewIngredient.bind(this);
  }

  componentDidMount() {
    API.units().getAll().then(res => {
      this.setState({
        unitList: res.data.data,
      })
    })
  }

  createNewIngredient() {
    var ingredient = {
      id: "",
      name: this.state.name,
      kcal: this.state.kcal,
      price: this.state.price,
      unit: {
        id: this.state.unit_id,
        name:"",
      }
    }
    let selectedUnit = this.state.unitList.filter(function(unit){
      return unit.id == ingredient.unit.id;
    });
    API.ingredients().create(ingredient, this.props.auth_token).then(res => {
      ingredient.id = res.data.data[0].id;
      this.setState({
        id: res.data.data[0].id,
      }, () => {
        ingredient.unit.name = selectedUnit[0].name;
        this.props.addIngredient(ingredient);
        this.setState({
          name:"",
          kcal:"",
          unit:"g",
          unit_id:1,
          id: 0
        })
      });
    });
  }

  render(e) {
    let unitList = this.state.unitList.map(unit => {
      return <option key={unit.id} value={unit.id}>{unit.name}</option>;
    });
    return (
      <div className="modal fade" id="newIngredientModal" tabIndex="-1" role="dialog" aria-labelledby="newIngredientModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Ajoutez un ingrédient disponible</h5>
              <button type="button" className="close" data-dismiss="modal" data-toggle="modal" data-target="#ingredientsModal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body container form-group">
              <input className="form-control mb-2" type="text" value={this.state.name} placeholder="Entrez le nom de l'ingrédient*" onChange={e => this.setState({name: e.target.value})}/>
              <select className="form-control mb-2" placeholder="Entrez l'unité de mesure de votre ingrédient" onChange={e => {
                    this.setState({unit: "", unit_id: e.target.value})
                }}>
                {unitList}
              </select>
              <input className="form-control mb-2" type="number" value={this.state.kcal} placeholder="Entrez l'apport calorique pour une unité (kcal)" onChange={e => this.setState({kcal: e.target.value})}/>
              <label className="pr-2 pl-2">
                <input type="radio" value="low" checked={this.state.price === "low"} onChange={e => this.setState({price: e.target.value})} />
                Prix faible
              </label>
              <label className="pr-2 pl-2">
                <input type="radio" value="middle" checked={this.state.price === "middle"} onChange={e => this.setState({price: e.target.value})} />
                Prix moyen
              </label>
              <label className="pr-2 pl-2">
                <input type="radio" value="high" checked={this.state.price === "high"} onChange={e => this.setState({price: e.target.value})} />
                Prix élevé
              </label>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal" data-toggle="modal" data-target="#ingredientsModal">Annuler</button>
              <button type="button" className="btn btn-primary" onClick={this.createNewIngredient} data-dismiss="modal" data-toggle="modal" data-target="#ingredientsModal">Ajouter cet ingrédient</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token,
  };
}

export default connect(mapStateToProps)(NewIngredient);
