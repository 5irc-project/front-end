import React, { Component } from "react";
import styles from "./RecipeCreation.module.css";
import cn from "classnames";
import { connect } from 'react-redux';
import { updateCreatedRecipe } from '../../../actions';

import ImageDropzone from "../components/ImageDropzone";
import TagSelection from "../components/TagSelection";
import IngredientSelection from "../components/IngredientSelection";
import StepSelection from "../components/StepSelection";
import Rating from '../../common/Rating';
import API from '../../../services/api.js';

class RecipeCreation extends Component {
  constructor(props){
    super(props);
    this.state= {
      recipeCreation: {
      }
    }
    this.changeProperty = this.changeProperty.bind(this);
    this.createRecipe = this.createRecipe.bind(this);
  }

  changeProperty(e, property) {
    let recipeCreation = "";
    // check if the property was already added to nested objects
    if(this.state.recipeCreation[property] || this.state.recipeCreation[property] === ''){
      // if so just change its value
      recipeCreation = {...this.state.recipeCreation};
      recipeCreation[property] = e.target.value;
    }
    else {
      // else create a new nested object
      recipeCreation = Object.assign({[property]: e.target.value}, this.state.recipeCreation);
    }
    this.setState({recipeCreation});
  }

  createRecipe(e) {
    e.preventDefault();
    console.log(this.state.recipeCreation);
    API.recipes().create(this.state.recipeCreation, this.props.auth_token).then((res) => {
      this.props.changeCreationStatus()
    });
  }

  render() {
    return(
      <div className={cn(styles.verticallyScrollable,"border-top h-100 p-3")}>
        <form>
          <div className="container m-2">
            <div className="row">
              <span className={cn(styles.previous, "m-2 p-2")} onClick={this.props.changeCreationStatus}>&laquo; Retour</span>
              <ImageDropzone onChange={this.changeProperty}/>
            </div>
            <div className="row pt-2">
              <div className="col-9">
                <div className="row">
                  <input className="form-control form-control-lg col-10" id="titleInput" type="text" placeholder="Titre"
                    onChange={(e) => this.changeProperty(e, "name")}/>
                </div>
                <div className="row justify-content-between pt-2">
                  <div className="col-1">
                    <Rating score="0" isDisabled="true"/>
                  </div>
                  <div className="col-10">
                    <textarea className="form-control" id="descriptionInput" placeholder="Description" rows="4" resize="none"
                      onChange={(e) => this.changeProperty(e, "description")}/>
                  </div>
                </div>
              </div>
              <div className="col-3">
                <TagSelection onChange={this.changeProperty} tags={this.state.recipeCreation.tagRecipes}/>
              </div>
            </div>
            <div className="row">
              <div className="col-3 pt-3">
                <IngredientSelection onChange={this.changeProperty} ingredients={this.state.recipeCreation.ingredientrecipe}/>
              </div>
              <div className="col-9 pt-3">
                <StepSelection onChange={this.changeProperty} steps={this.state.recipeCreation.steps}/>
              </div>
            </div>
          </div>
          <button className={cn("btn btn-primary", styles.validateButton)} onClick={this.createRecipe}>
            Mettre en ligne
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    recipe: state.recipeReducer.created_recipe,
    auth_token: state.authReducer.token,
  };
}

export default connect(mapStateToProps)(RecipeCreation);
