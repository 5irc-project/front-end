import React, { Component } from "react";
import cn from 'classnames';
import styles from './LeftPanel.module.css';
import { setRecipesList, setIsRecipesListOk, setSelectedRecipe } from "../../actions";
import { connect } from "react-redux";

import API from "../../services/api.js";

class LeftPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: []
    }
    this.filterByTags = this.filterByTags.bind(this);
    this.getAllRecipes = this.getAllRecipes.bind(this);
  }

  componentDidMount() {
    API.tags().getAll().then(res => {
      this.setState({
        tags: res.data.data
      })
    })
  }

  filterByTags(e, tag) {
    this.props.dispatch(setSelectedRecipe(null));
    this.props.dispatch(setRecipesList(undefined));
    API.recipes().tagFilter(tag.id, this.props.auth_token).then(res => {
      if (res.status === 204) {
        this.props.dispatch(setRecipesList([]))
      }
      else {
        this.props.dispatch(setRecipesList(res.data.data));
      }
    })
  }

  getAllRecipes() {
    this.props.dispatch(setSelectedRecipe(null));
    this.props.dispatch(setRecipesList(undefined));
    API.recipes().getAll(this.props.auth_token).then(res => {
      this.props.dispatch(setRecipesList(res.data.data));
    })
  }

  render() {
    const themes = this.state.tags.map(tag => {
      return(
        <li className="nav-item pl-2" key={tag.id}>
          <a className="nav-link" href="#" onClick={(e) => this.filterByTags(e, tag)}>
            {tag.name}
          </a>
        </li>
      );
    })
    return (
      <div className="h-100 border-right bg-light">
        <nav id="sidebar">
          <ul className="pt-2 nav flex-column">
            <li className="nav-item">
              <a className="nav-link active" href="#" onClick={this.getAllRecipes}>
                Accueil
              </a>
            </li>
            <h5 className="sidebar-heading d-flex justify-content-between align-items-center px-3 pt-2 pb-2 mb-0 text-muted">
              Thèmes
            </h5>
              {themes}
          </ul>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth_token: state.authReducer.token,
    recipes_list: state.recipesListReducer.recipes_list,
  };
}

export default connect(mapStateToProps)(LeftPanel);
